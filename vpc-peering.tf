# Requester's side of the connection.
resource "aws_vpc_peering_connection" "peer" {
  peer_vpc_id = module.vpc_for_eks_west1.vpc_id
  vpc_id      = module.vpc_for_eks_east1.vpc_id


  tags = {
    Name = "VPC Peering between west1 and east1"
    Side = "Requester"
  }

  peer_region = "us-west-1"
  provider    = aws.east1
}

# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "acceptor" {
  provider                  = aws.west1
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  auto_accept               = true
  tags = {
    Side = "Accepter"
  }
}