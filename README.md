
# Gitlab CI Pipeline
```
Stages/PHASE:
- BUILD - Auto Run ( INT, VALIDATE, PLAN,) - Manual (APPLY)
- INSTALL - deploy's EFS CSI Storage
- KUBEFED - Cluster Setup using Kubefed
- DESTROY - Destroy the TF infrastructure
```


# Cluster DEMO - Kubectl
```
aws eks update-kubeconfig --name eks-east --region us-east-1 
aws eks update-kubeconfig --name eks-west --region us-west-1

kubectl config rename-context $(kubectl config get-contexts --no-headers=true -o name | grep us-east-1:685252273268) kubefed-cluster.us-east-1
kubectl config rename-context $(kubectl config get-contexts --no-headers=true -o name | grep us-west-1:685252273268) kubefed-cluster.us-west-1
kubectl config get-contexts

kubectl config use-context kubefed-cluster.us-west-1
kubectl config use-context kubefed-cluster.us-east-1
```

# Deployment DEMO - Kubectl / Lens
```
# Create namespace kubefed-minilab
kubectl create namespace kubefed-nginx

# Federate the namespace 
kubefedctl federate namespace kubefed-nginx

# Check is ns federated to all clusters
kubectl get federatednamespace kubefed-nginx -n kubefed-nginx -o=json | jq '.status.clusters'

# Create FederatedDeployment/FederatedService
kubefedctl federate -f deployment.yaml > federated_deployment.yaml
kubefedctl federate -f service.yaml > federated_service.yaml

kubectl apply -f federated_deployment.yaml
kubectl apply -f federated_service.yaml

# Verify using Lens or kubectl
```

