variable "eks_cluster_name" {
  description = "The name of the EKS cluster"
  type = string
}

variable "efs_tag_name" {
  type        = string
  description = "Name tag for the EFS"
}

variable "efs_description" {
  type        = string
  default     = "main"
  description = "Route table description"
}

variable vpc_id {
  description = "VPC ID from which belogs the subnets"
  type        = string
}

variable efs_creation_token {
  description = "EFS Creation Token"
  type        = string
}

variable efs_performance_mode {
  description = "EFS Performance Mode"
  type        = string
}

variable efs_throughput_mode {
  description = "EFS Throughput Mode"
  type        = string
}

variable efs_encrypted {
  description = "EFS Encryption "
  type        = string
}

variable "private_subnet_ids" {
  type = list(string)
  description = "List of private subnet IDs."
}

variable eks_sg {
  description = "EFS Throughput Mode"
  default = "eks-sg"
  type        = string
}