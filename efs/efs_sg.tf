resource "aws_security_group" "allow_eks_cluster" {
  name        = "efs-east-sg"
  description = var.efs_description
  vpc_id      = var.vpc_id

  ingress {
    description = "NFS For EKS Cluster "
    # from_port   = 2049
    # to_port     = 2049
    # protocol    = "tcp"
    # security_groups = [
    #   data.terraform_remote_state.cluster.outputs.eks_cluster_sg_id
    # ]
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"] 
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.efs_tag_name
  }
}