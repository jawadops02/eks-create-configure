
// efs.tf
resource "aws_efs_file_system" "efs-aws" {
   creation_token   = var.efs_creation_token
   performance_mode = var.efs_performance_mode
   throughput_mode  = var.efs_throughput_mode
   encrypted        = var.efs_encrypted
    tags = {
        Name = var.efs_tag_name
    }
 }

 // efs.tf (continued)
resource "aws_efs_mount_target" "efs-mt-example" {
    count             = length(var.private_subnet_ids)
    file_system_id    = "${aws_efs_file_system.efs-aws.id}"
    subnet_id         = var.private_subnet_ids[count.index]
    security_groups   = ["${aws_security_group.allow_eks_cluster.id}"]
 }

 resource "aws_efs_access_point" "efseks" {
  file_system_id = "${aws_efs_file_system.efs-aws.id}"
}