# VPC for EKS

module "vpc_for_eks_east1" {
  source                     = "./vpc"
  eks_cluster_name           = var.cluster_name_east
  vpc_tag_name               = "${var.cluster_name_east}-vpc"
  vpc_cidr_block             = var.vpc_cidr_block_east1
  private_subnet_cidr_blocks = var.private_subnet_cidr_blocks_east1
  public_subnet_cidr_blocks  = var.public_subnet_cidr_blocks_east1
  private_subnet_tag_name    = var.public_subnet_tag_name_east1
  public_subnet_tag_name     = var.private_subnet_tag_name_east1
  availability_zones         = var.availability_zones_east1
  providers = {
    aws = aws.east1
  }
}

module "efs_for_eks_east1" {
  source               = "./efs"
  eks_cluster_name     = var.cluster_name_east
  vpc_id               = module.vpc_for_eks_east1.vpc_id
  efs_tag_name         = "${var.cluster_name_east}-efs"
  efs_description      = "This will allow the cluster to access this volume and use it."
  efs_creation_token   = "efs-east"
  efs_performance_mode = var.efs_performance_mode
  efs_throughput_mode  = var.efs_throughput_mode
  efs_encrypted        = var.efs_encrypted
  private_subnet_ids   = module.vpc_for_eks_east1.private_subnet_ids
  providers = {
    aws = aws.east1
  }
}

# EKS Cluster
module "eks_cluster_and_worker_nodes_east" {
  source = "./eks"
  # Cluster

  vpc_id                 = module.vpc_for_eks_east1.vpc_id
  cluster_sg_name        = "${var.cluster_name_east}-cluster-sg"
  nodes_sg_name          = "${var.cluster_name_east}-node-sg"
  eks_cluster_name       = var.cluster_name_east
  eks_cluster_subnet_ids = flatten([module.vpc_for_eks_east1.public_subnet_ids, module.vpc_for_eks_east1.private_subnet_ids])
  # Node group configuration (including autoscaling configurations)
  pvt_desired_size        = var.pvt_desired_size_east1
  pvt_max_size            = var.pvt_max_size_east1
  pvt_min_size            = var.pvt_min_size_east1
  pblc_desired_size       = var.pblc_desired_size_east1
  pblc_max_size           = var.pblc_max_size_east1
  pblc_min_size           = var.pblc_min_size_east1
  endpoint_private_access = true
  endpoint_public_access  = true
  node_group_name         = "${var.cluster_name_east}-node-group"
  private_subnet_ids      = module.vpc_for_eks_east1.private_subnet_ids
  public_subnet_ids       = module.vpc_for_eks_east1.public_subnet_ids
  instance_types          = var.instance_types_east1
  providers = {
    aws = aws.east1
  }
}

