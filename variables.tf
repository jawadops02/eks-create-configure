

variable "cluster_name_west" {
  description = "EKS cluster name"
  default     = "eks-west"
  type        = string
}

variable "cluster_name_east" {
  description = "EKS cluster name"
  default     = "eks-east"
  type        = string
}

variable "region" {
  description = "AWS region to deploy to"
  default     = "us-west-1"
  type        = string
}


### EAST VARS


variable "instance_types_east1" {
  type        = list(string)
  default     = ["t2.large"]
  description = "Set of instance types associated with the EKS Node Group."
}


variable "pvt_desired_size_east1" {
  description = "Desired number of worker nodes in private subnet"
  default     = 1
  type        = number
}

variable "pvt_max_size_east1" {
  description = "Maximum number of worker nodes in private subnet."
  default     = 5
  type        = number
}

variable "pvt_min_size_east1" {
  description = "Minimum number of worker nodes in private subnet."
  default     = 1
  type        = number
}

variable "pblc_desired_size_east1" {
  description = "Desired number of worker nodes in public subnet"
  default     = 1
  type        = number
}

variable "pblc_max_size_east1" {
  description = "Maximum number of worker nodes in public subnet."
  default     = 2
  type        = number
}

variable "pblc_min_size_east1" {
  description = "Minimum number of worker nodes in public subnet."
  default     = 1
  type        = number
}

variable "route_table_tag_name_east1" {
  type        = string
  default     = "main"
  description = "Route table description"
}

variable "vpc_cidr_block_east1" {
  type        = string
  default     = "10.1.0.0/16"
  description = "CIDR block range for vpc"
}

variable "private_subnet_cidr_blocks_east1" {
  type        = list(string)
  default     = ["10.1.0.0/24", "10.1.1.0/24"]
  description = "CIDR block range for the private subnet"
}

variable "public_subnet_cidr_blocks_east1" {
  type        = list(string)
  default     = ["10.1.2.0/24", "10.1.3.0/24"]
  description = "CIDR block range for the public subnet"
}

variable "private_subnet_tag_name_east1" {
  type        = string
  default     = "Kubefed private subnet"
  description = "Name tag for the private subnet"
}

variable "public_subnet_tag_name_east1" {
  type        = string
  default     = "Kubefed public subnet"
  description = "Name tag for the public subnet"
}

variable "availability_zones_east1" {
  type = list(string)
  # default = ["eu-west-1a", "eu-west-1b"]
  default     = ["us-east-1a", "us-east-1b"]
  description = "List of availability zones for the selected region"
}


### WEST1 VARS



variable "instance_types_west1" {
  type        = list(string)
  default     = ["t2.large"]
  description = "Set of instance types associated with the EKS Node Group."
}


variable "pvt_desired_size_west1" {
  description = "Desired number of worker nodes in private subnet"
  default     = 1
  type        = number
}

variable "pvt_max_size_west1" {
  description = "Maximum number of worker nodes in private subnet."
  default     = 2
  type        = number
}

variable "pvt_min_size_west1" {
  description = "Minimum number of worker nodes in private subnet."
  default     = 1
  type        = number
}

variable "pblc_desired_size_west1" {
  description = "Desired number of worker nodes in public subnet"
  default     = 1
  type        = number
}

variable "pblc_max_size_west1" {
  description = "Maximum number of worker nodes in public subnet."
  default     = 2
  type        = number
}

variable "pblc_min_size_west1" {
  description = "Minimum number of worker nodes in public subnet."
  default     = 1
  type        = number
}

variable "route_table_tag_name_west1" {
  type        = string
  default     = "main"
  description = "Route table description"
}

variable "vpc_cidr_block_west1" {
  type        = string
  default     = "10.0.0.0/16"
  description = "CIDR block range for vpc"
}

variable "private_subnet_cidr_blocks_west1" {
  type        = list(string)
  default     = ["10.0.0.0/24", "10.0.1.0/24"]
  description = "CIDR block range for the private subnet"
}

variable "public_subnet_cidr_blocks_west1" {
  type        = list(string)
  default     = ["10.0.2.0/24", "10.0.3.0/24"]
  description = "CIDR block range for the public subnet"
}

variable "private_subnet_tag_name_west1" {
  type        = string
  default     = "Kubefed private subnet"
  description = "Name tag for the private subnet"
}

variable "public_subnet_tag_name_west1" {
  type        = string
  default     = "Kubefed public subnet"
  description = "Name tag for the public subnet"
}

variable "availability_zones_west1" {
  type    = list(string)
  default = ["us-west-1a", "us-west-1c"]
  # default = ["us-east-1a", "us-east-1b"]
  description = "List of availability zones for the selected region"
}


## backend conf

variable "s3_backend_bucket" {
  type        = string
  default     = "core10-kubefed-terraform-state"
  description = "bucket name for backend"
}

variable "s3_backend_key" {
  type        = string
  default     = "core10-kubefed-terraform/terraform.tfstate"
  description = "bucket key/prefix for backend"
}

variable "s3_backend_region" {
  type        = string
  default     = "us-east-1"
  description = "s3 backend region"
}

variable "s3_backend_dynamodb_table" {
  type        = string
  default     = "core10-kubefed-tf-state"
  description = "s3 backend dyanodb table name"
}


### EFS VARS


variable "efs_performance_mode" {
  description = "EFS Performance Mode"
  default     = "generalPurpose"
  type        = string
}

variable "efs_throughput_mode" {
  description = "EFS Throughput Mode"
  default     = "bursting"
  type        = string
}

variable "efs_encrypted" {
  description = "EFS Encryption"
  default     = "true"
  type        = string
}
